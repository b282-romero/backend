[
  {
    "id": 1,
    "name": "X",
    "description": "X",
    "price": 202.50,
    "stocks": 20,
    "isActive": true,
    "sku": "XYZ12345"
  },
  {
    "id": 2,
    "name": "Y",
    "description": "Y",
    "price": 202.50,
    "stocks": 10,
    "isActive": true,
    "sku": "XYZ12344"
  },
  {
    "id": 3,
    "name": "Z",
    "description": "Z",
    "price": 303.69,
    "stocks": 30,
    "isActive": false,
    "sku": "XYZ12343"
  }
]