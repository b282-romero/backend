[
  {
    "id": 1,
    "userId": 1,
    "transactionDate": "2023-06-15",
    "status": "completed",
    "total": 101.00
  },
  {
    "id": 2,
    "userId": 2,
    "transactionDate": "2023-06-15",
    "status": "in progress",
    "total": 202.50
  },
  {
    "id": 3,
    "userId": 3,
    "transactionDate": "2023-06-15",
    "status": "canceled",
    "total": 303.69
  }
]