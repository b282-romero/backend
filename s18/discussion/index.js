function printInput(){
    let nickname = prompt("Enter your nickname:");
    console.log("Hi, " + nickname);
}
printInput();

// Parameters and Arguments

function printName(name){
    console.log("My name is " + name);
}
printName("Juana");
printName("John");
printName("Jane");

let sampleVariable = "Yui";
printName(sampleVariable);

function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log("The remainder of " + num + "divided by 8 is: " + remainder);
    let isDivisibleBy8 = remainder === 0;
    console.log("Is " + num + "divisible by 8?");
    console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(65);
checkDivisibilityBy8(28);

// Functions as Arguments
function argumentFunction() {
    console.log("This function was passed as an argument before the message was printed.")
}

function invokeFunction(argumentFunction){
    argumentFunction();
}
invokeFunction(argumentFunction);
console.log(argumentFunction);

function createFullName(firstName, middleName, lastName) {
    console.log(firstName + ' ' + middleName + ' ' + lastName);
}
createFullName('Juan', 'Dela', 'Cruz');
createFullName('Juan', 'Dela', 'Cruz', "Hello");
createFullName('Juan', 'Dela');

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

function createFullName(middleName, firstName, lastName){
    
}